/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: FramePrincipal.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package vue;

import commandes.LoadCommand;
import controleur.*;
import modele.*;


import javax.swing.*;
import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class implements the global container view, the frame which contains the toolbar and the three image panels.
 *
 */
public class FramePrincipal extends JFrame implements Serializable {

	/**
	 * The title displayed by the application window.
	 */
	private static final String TITLE = "Laboratoire 2";
	/**
	 * The dimensions of the whole application window.
	 */
	private static final Dimension TOTAL_FRAME_DIMENSION = new Dimension(1600, 850);
	/**
	 * The dimensions of the images container.
	 */
	private static final Dimension IMAGE_CONTAINER_DIMENSION = new Dimension(1200, 750);
	/**
	 * The number of views, including the thumbnail and the perspectives.
	 */
	private static final int VIEWS_COUNT = 3;

	/**
	 * The list of the three models.
	 */
	public ArrayList<IModele> models = new ArrayList<IModele>();
	/**
	 * The main panel.
	 */
	private final JPanel panneauAffichage = new JPanel();
	/**
	 * The container panel that contains the 3 view panels.
	 */
	JPanel menuBarsPanel = new JPanel(new BorderLayout());
	/**
	 * The panel containing the three image view panels.
	 */
	JPanel viewsPanel = new JPanel(new GridLayout(1, VIEWS_COUNT));
	/**
	 * The toolbar controler.
	 */
	private ControleurBarreOutils controleurBarreOutils = new ControleurBarreOutils(this);
	/**
	 * The toolbar view.
	 */
	private BarreOutils barreOutils = new BarreOutils(this, controleurBarreOutils);
	/**
	 * The thumbnail model.
	 */
	private final PerpectiveStatiqueModel psm = new PerpectiveStatiqueModel();
	/**
	 * The perspective model 1 (center).
	 */
	private final PerspectiveDynamiqueModel pdm1 = new PerspectiveDynamiqueModel();
	/**
	 * The perspective model 2 (right).
	 */
	private final PerspectiveDynamiqueModel pdm2 = new PerspectiveDynamiqueModel();
	/**
	 * The thumbnail controler.
	 */
	private final ControleurVueStatique controleurVueStatique = new ControleurVueStatique(psm);
	/**
	 * The perspective controler 1 (center).
	 */
	private final ControleurVueDynamique controleurVueDyn1 = new ControleurVueDynamique(pdm1);
	/**
	 * The perspective controler 2 (right).
	 */
	private final ControleurVueDynamique controleurVueDyn2 = new ControleurVueDynamique(pdm2);
	/**
	 * The thumbnail view.
	 */
	private final PanneauVueStatique panneauVueStatique = new PanneauVueStatique(controleurVueStatique);
	/**
	 * The perspective view 1 (center).
	 */
	private final PanneauVueDynamique panneauVueDyn1 = new PanneauVueDynamique(controleurVueDyn1, pdm1);
	/**
	 * The perspective view 2 (right).
	 */
	private final PanneauVueDynamique panneauVueDyn2 = new PanneauVueDynamique(controleurVueDyn2, pdm2);
	/**
	 * The main controler, that contains the other controlers.
	 */
	private final MainControleur mainControleur = new MainControleur();

	/**
	 * Constructor.
	 */
	public FramePrincipal() {
		this.setVisible(true);
		this.setTitle(TITLE);
		this.setSize(TOTAL_FRAME_DIMENSION);
		setLayout(new BorderLayout());
		
		add(panneauAffichage, null);

		setVisible(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		psm.attachObserver(panneauVueStatique);
		pdm1.attachObserver(panneauVueDyn1);
		pdm2.attachObserver(panneauVueDyn2);

		controleurBarreOutils.setBarreOutils(barreOutils);
		controleurVueDyn1.setVueDyn(panneauVueDyn1);
		controleurVueDyn2.setVueDyn(panneauVueDyn2);

		mainControleur.setStatiqueControleur(controleurVueStatique);
		mainControleur.addPerspectiveControleur(controleurVueDyn1);
		mainControleur.addPerspectiveControleur(controleurVueDyn2);

		models.add(psm);
		models.add(pdm1);
		models.add(pdm2);

		initialisationFenetre();
	}

	/**
	 * Reinitialize the three models.
	 */
	public void reinitializeModels()
	{
		psm.reinitialize();
		pdm1.reinitialize();
		pdm2.reinitialize();
	}

	/**
	 * Creates the main window.
	 */
	public void initialisationFenetre() {
		this.setContentPane(panneauAffichage);
		panneauAffichage.setLayout(new BorderLayout());
		panneauAffichage.setPreferredSize(IMAGE_CONTAINER_DIMENSION);
		menuBarsPanel.add(barreOutils, BorderLayout.NORTH);
		panneauAffichage.add(menuBarsPanel, BorderLayout.NORTH);

		// Add the 3 image panel to the image container
		viewsPanel.add(panneauVueStatique);
		viewsPanel.add(panneauVueDyn1);
		viewsPanel.add(panneauVueDyn2);

		// Add the image container into the main panel
		panneauAffichage.add(viewsPanel);
	}

	/**
	 * Returns the three models.
	 *
	 * @return The models as an arraylist.
	 */
	public ArrayList<IModele> getModels() {
		return models;
	}

	/**
	 * Returns the main controler.
	 *
	 * @return The main controler.
	 */
	public MainControleur getMainControleur() {
		return mainControleur;
	}
}


