/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: IGlobalCommand.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/

package commandes;

import vue.FramePrincipal;

/**
 * This interface provides the methods for a command that comes from the global application,
 * that is, a command that affects the whole application.
 *
 */
public interface IGlobalCommand
{
    /**
     * The method to execute the command.
     *
     * @param framePrincipal The main frame that contains the whole application GUI.
     */
    void execute(FramePrincipal framePrincipal);
}
