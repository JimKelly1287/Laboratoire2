/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: ZoomCommand.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package commandes;

import modele.PerspectiveDynamiqueModel;
import vue.PanneauVueDynamique;

/**
 * This class is responsible for the zoom in/out command.
 */
public class ZoomCommand implements ICommand {
	/**
	 * The active dynamic view.
	 */
	private PanneauVueDynamique vueDyn;
	/**
	 * The signed (+/-) zoom scale for the image.
	 */
	private final int signedZoomScale;

	/**
	 * Constructor.
	 *
	 * @param vueDyn          The view that contains the image to edit.
	 * @param signedZoomScale The zoom scale to zoom in or zoom out.
	 */
	public ZoomCommand(PanneauVueDynamique vueDyn, int signedZoomScale) {
		this.vueDyn = vueDyn;
		this.signedZoomScale = signedZoomScale;
	}

	/**
	 * The method to execute the zoom command.
	 */
	@Override
	public void execute() {
		PerspectiveDynamiqueModel dynModel = vueDyn.getDynModel();

		// Set the new dimensions
		int imgWidth = dynModel.getLargeur() + this.signedZoomScale;
		int imgHeight = dynModel.getHauteur() + this.signedZoomScale;

		dynModel.updateImageSize(imgWidth, imgHeight);

	}

	/**
	 * The method to undo the zoom command.
	 *
	 */
	@Override
	public void undo() {
		PerspectiveDynamiqueModel dynModel = vueDyn.getDynModel();

		// Set the new dimensions
		int imgWidth = dynModel.getLargeur() - this.signedZoomScale;
		int imgHeight = dynModel.getHauteur() - this.signedZoomScale;

		dynModel.updateImageSize(imgWidth, imgHeight);
	}
}