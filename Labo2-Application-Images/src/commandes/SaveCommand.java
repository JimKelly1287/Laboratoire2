/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: SaveCommand.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package commandes;

import modele.IModele;
import vue.FramePrincipal;
import javax.swing.*;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * This class is responsible for the implementation for the save command.
 *
 */
public class SaveCommand implements IGlobalCommand
{
	/**
	 * Constructor.
	 */
	public SaveCommand(){}
	/**
	 * Demande à l'utilisateur d'entrer un nom de fichier ainsi que l'endroit
	 * où il veut le sauvegarder. Va ensuite le sérializer.
	 *
	 * @param framePrincipal The main frame that contains the whole application GUI.
	 */
	@Override
	public void execute(FramePrincipal framePrincipal)
	{
		if (framePrincipal == null)
			return;

		JFileChooser fileChooser = new JFileChooser();
		int result = fileChooser.showSaveDialog(null);

		if(result == JFileChooser.APPROVE_OPTION)
		{
			java.io.File selectedFile = fileChooser.getSelectedFile();

			//append file extension if not already present
			if(!selectedFile.getName().toLowerCase().endsWith(".dat"))
			{
				selectedFile = new java.io.File(selectedFile.getAbsolutePath() + ".dat");
			}

			System.out.println("Le nom du selectedFile="+selectedFile);

			try// (ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(selectedFile)))
			{
				FileOutputStream fileOut = new FileOutputStream(selectedFile);
				ObjectOutputStream out = new ObjectOutputStream(fileOut);

				ArrayList<IModele> modeles = framePrincipal.models;

				// Utile pour debug purpose
//				for(IModele tmpModel: modeles)
//				{
//					System.out.println("*********************** AT SAVE COMMAND() ************************");
//					System.out.println("Classe = "+ tmpModel.getClass().getSimpleName());
//					System.out.println("PATH= "+tmpModel.getImagePath());
//					System.out.println("Position x = " + tmpModel.getPosition().x);
//					System.out.println("Position y = " + tmpModel.getPosition().y);
//					System.out.println("Hauteur =" + tmpModel.getHauteur());
//					System.out.println("Largeur = " + tmpModel.getLargeur());
//				}

				out.writeObject(modeles);
				out.close();
				fileOut.close();
			}
			catch (IOException e)
			{
				e.printStackTrace();
				System.out.println("Erreur dans la sauvegarde");
			}
		}
		else
		{
			System.out.println("Entrez quelque chose");
		}
	}
}
