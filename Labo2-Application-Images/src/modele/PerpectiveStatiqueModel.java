/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: PerpectiveStatiqueModel.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/

package modele;

import observer.Observable;

import java.awt.*;
import java.io.Serializable;

/**
 * This class implements the model for the thumbnail image.
 *
 */
public class PerpectiveStatiqueModel extends Observable implements IModele, Serializable
{
	private static final long serialVersionUID = 3646134082515730001L;
	/**
	 * The image path.
	 */
	private String imagePath;
	/**
	 * The current position of the image.
	 */
	private Point position = new Point(0, 0);
	/**
	 * The height of the image.
	 */
	private int hauteur;
	/**
	 * The width of the image.
	 */
	private int largeur;
	/**
	 * The image.
	 */
	private transient Image image;
	/**
	 * Indicates if the image is new.
	 */
	private transient boolean isNewImage = false;

	/**
	 * Constructor.
	 */
	public PerpectiveStatiqueModel(){}

	/**
	 * Updates the image height, width and path only.
	 *
	 * @param path The new image path to assign.
	 * @param image The new image.
	 */
	public void update(String path, Image image)
	{
		this.imagePath = path;
		this.hauteur = image.getHeight(null);
		this.largeur = image.getWidth(null);
	}

	/**
	 * Display the image.
	 *
	 * @param nouvelleImage The image to display.
	 */
	public void afficher(Image nouvelleImage)
	{
		if (this.image == null) {
			this.image = nouvelleImage;
			isNewImage = true;
			notifyObservers(this);

		} else {
			this.image = nouvelleImage;
			notifyObservers(this);
		}
	}

	/**
	 * Returns the height of the image.
	 *
	 * @return The height of the image.
	 */
	@Override
	public int getHauteur() {
		return hauteur;
	}
	/**
	 * Assigns a value to the height.
	 *
	 * @param hauteur The int value of the height.
	 */
	@Override
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	/**
	 * Returns the width of the image.
	 *
	 * @return The int value of the width.
	 */
	@Override
	public int getLargeur() {
		return largeur;
	}
	/**
	 * Assigns a value to the width.
	 *
	 * @param largeur The int value of the width.
	 */
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}

	/**
	 * Returns the image for this model.
	 *
	 * @return The image.
	 */
	public Image getImage() {
		return image;
	}
	/**
	 * Assigns a value to the image.
	 *
	 * @param image The image.
	 */
	public void setImage(Image image) {
		this.image = image;
	}
	/**
	 * This returns the absolute path of the image.
	 *
	 * @return The image absolute path.
	 *
	 */
	@Override
	public String getImagePath() {
		return imagePath;
	}
	/**
	 * This method sets the image absolute path.
	 *
	 * @param imagePath The absolute path of the image.
	 *
	 */
	@Override
	public void setImagePath(String imagePath) {
		this.imagePath = imagePath;
	}
	/**
	 * This returns the position of the image.
	 *
	 * @return The point representing the position.
	 *
	 */
	@Override
	public Point getPosition() {
		return position;
	}
	/**
	 * Assigns a value to the image position.
	 *
	 * @param position The position of the image
	 * @param notifyObserversNow Indicates if the observers must be notified.
	 */@Override
	public void setPosition(Point position, boolean notifyObserversNow) {
		this.position = position;
	}

	/**
	 * The customized string value of the model.
	 *
	 * @return The string representing the model.
	 */
	@Override
	public String toString() {
		return "PerpectiveStatiqueModel{" +
				"hauteur=" + hauteur +
				", largeur=" + largeur +
				", image=" + image+
				", isNewImage=" + isNewImage +
				'}';
	}
	/**
	 * Method that reinitialize the dimensions and position of the image.
	 */
	@Override
	public void reinitialize()
	{
		this.imagePath = null;
		this.position = new Point(0,0);
		this.hauteur = 0;
		this.largeur = 0;
		this.image = null;
		this.isNewImage = false;
	}
}
