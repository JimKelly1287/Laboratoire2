/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: MainControleur.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/

package controleur;

import commandes.GestionnaireCommandes;
import commandes.ICommand;
import commandes.ZoomCommand;
import modele.IModele;
import modele.PerspectiveDynamiqueModel;
import modele.PerpectiveStatiqueModel;
import vue.FramePrincipal;
import vue.PanneauVueDynamique;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * This class is a Controleurs container and contains static attributes and methods.
 * It contains all the controleurs and handles general processes
 * like activating/deactivating views.
 */
public class MainControleur implements Serializable
{
	/**
	 * Contains the controleurs of perspective 1 and perspective 2
	 */
	private static ArrayList<ControleurVueDynamique> perspectiveControleurs = new ArrayList<ControleurVueDynamique>();
    /**
     * The thumbnail view controler.
     */
	private static ControleurVueStatique controleurVueStatique;
//    private ControleurBarreOutils barreOutilControleur; //if needed

	/**
	 * Constructor
	 */
	public MainControleur() {
	}

	/**
	 * Detects and registers the active view where the mouse event happened.
	 *
	 * @param vueDyn The active view where the mouse event happened.
	 */
	public static void setActiveView(PanneauVueDynamique vueDyn) {
		for (ControleurVueDynamique contr : perspectiveControleurs) {
			PerspectiveDynamiqueModel currModel = contr.getDynModel();
			PanneauVueDynamique currVue = contr.getVueDyn();

			if (currVue == vueDyn) {// This is the clicked (active) view.
				currModel.setRepresentsActiveView(true);
				currVue.setCurrBorder(true);

			} else { // This view will be inactivated.
				currModel.setRepresentsActiveView(false);
				currVue.setCurrBorder(false);
			}
		}
	}

    /**
     * The event handler when the mouse is pressed (not released).
     *
     * @param e The mouse pressed event to be processed.
     * @param vueDyn The perspective view.
     */
	public static void onMousePress(MouseEvent e, PanneauVueDynamique vueDyn)
    {
		setActiveView(vueDyn);
	}
    /**
     * The event handler when the mouse wheeled is rolled.
     *
     * @param e The mouse wheel event to be processed.
     * @param vueDyn The perspective view.
     */
    public static void onMouseWheel(MouseWheelEvent e, PanneauVueDynamique vueDyn) {
        setActiveView(vueDyn);
    }
    /**
     * Assigns a value to the thumbnail controler.
     *
     * @param controleur The thumbnail controler.
     */
    public static void setStatiqueControleur(ControleurVueStatique controleur)
    {
        controleurVueStatique = controleur;
    }

//    /**
//     * Will undo a command, when pressing CTRL + Z
//     */
//    private void addKeyboardListener()
//    {
//        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new KeyEventDispatcher() {
//            @Override
//            public boolean dispatchKeyEvent(KeyEvent e)
//            {
//                if (e.getID() == KeyEvent.KEY_PRESSED
//                        && e.isControlDown()
//                        && e.getKeyCode() == KeyEvent.VK_Z
//                        && !GestionnaireCommandes.stackIsEmpty())
//                {
//                    ICommand lastCommand = GestionnaireCommandes.pop();
//                    lastCommand.undo();
//                }
//                return false;
//            }
//        });
//    }

    /**
     * Loads the .dat file containing the data to recreate the three image displayed.
     *
     * @param modeles An array containing the three models for each image panel.
     */
    public void loadImagesFromDatFile(ArrayList<IModele> modeles)
    {
        boolean isFirstDynamicView = true;

        for (int i = 0; i < modeles.size(); i++)
        {
            IModele tmpModel = modeles.get(i);

            if (tmpModel instanceof PerspectiveDynamiqueModel)
            {
                ControleurVueDynamique tmpControleur;

                if (isFirstDynamicView) {
                    tmpControleur = perspectiveControleurs.get(0);
                    isFirstDynamicView = false;
                }
                else
                {
                    tmpControleur = perspectiveControleurs.get(1);
                }

                String newImagePath = tmpModel.getImagePath();
                Point newPosition = tmpModel.getPosition();
                int newHauteur = tmpModel.getHauteur();
                int newLargeur = tmpModel.getLargeur();

                PerspectiveDynamiqueModel currModel = tmpControleur.getDynModel();
                currModel.setImagePath(newImagePath);
                currModel.setHauteur(newHauteur);
                currModel.setLargeur(newLargeur);
                currModel.setPosition(newPosition, false);
            }
            else // modele is a PerspectiveStatiqueModel
            {
                String newImagePath = tmpModel.getImagePath();
                Point newPosition = tmpModel.getPosition();
                int newHauteur = tmpModel.getHauteur();
                int newLargeur = tmpModel.getLargeur();

                PerpectiveStatiqueModel currModel = controleurVueStatique.getPerpectiveStatiqueModel();
                currModel.setImagePath(newImagePath);
                currModel.setHauteur(newHauteur);
                currModel.setLargeur(newLargeur);
                currModel.setPosition(newPosition, false);
            }
        }
    }

    /**
     * Adds a perspective controler to the perspective controlers.
     *
     * @param dynControleur The controler to add.
     */
	public static void addPerspectiveControleur(ControleurVueDynamique dynControleur) {
		perspectiveControleurs.add(dynControleur);
	}

}
