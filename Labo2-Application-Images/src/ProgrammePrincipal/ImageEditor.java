/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: ImageEditor.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 *******************************************************/
package ProgrammePrincipal;

import vue.FramePrincipal;

import java.awt.*;

/**
 * This class is the entry point of the application.
 *
 */
public class ImageEditor
{
	/**
	 * Constructor.
	 */
	public ImageEditor(){}

	/**
	 * The main function as in the entry point of the application.
	 *
	 * @param args The arguments from the command line.
	 */
	public static void main(String[] args) {
		FramePrincipal framePrincipal = new FramePrincipal();
	}
}