/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: PanneauVueDynamique.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package vue;

import controleur.*;
import modele.PerspectiveDynamiqueModel;
import observer.Observer;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.Serializable;

/**
 * This class implements the view panel for the two perspective images.
 *
 */
public class PanneauVueDynamique extends JPanel implements Observer, Serializable
{
	/**
	 * The associated controler.
	 */
	private ControleurVueDynamique dynControleur;
	/**
	 * The associated model.
	 */
	private PerspectiveDynamiqueModel dynModel;
	/**
	 * The border style when view is inactive.
	 */
	private Border inactiveViewBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);
	/**
	 * The border style when view is active.
	 */
	private Border activeViewBorder = BorderFactory.createLineBorder(Color.black);
	/**
	 * The current border.
	 */
	private Border currBorder = inactiveViewBorder;
	/**
	 * The image.
	 */
    private transient Image image; //unserializable

	/**
	 * Constructor.
	 *
	 * @param controleurVueDynamique The associated view.
	 * @param pdm The associated model.
	 */
	public PanneauVueDynamique(ControleurVueDynamique controleurVueDynamique, PerspectiveDynamiqueModel pdm)
	{
		this.dynControleur = controleurVueDynamique;
		this.dynModel = pdm;

		//Initialize the UI
		setLayout(new BorderLayout());
		setBorder(currBorder);

		this.setVisible(true);

		//Event listeners for mouse events
		addMouseListener(new MouseAdapter() {
			// Mouse press (mouse down) event
			@Override
			public void mousePressed(MouseEvent e) {
				controleurVueDynamique.onMousePress(e);
			}

			// Mouse release listener for translation
			@Override
			public void mouseReleased(MouseEvent e) {
				controleurVueDynamique.onMouseRelease(e);
			}
		});

		// Mouse wheel listener for the zoom.
		addMouseWheelListener(controleurVueDynamique::onMouseWheel);

		// Mouse motion listener for translation.
		addMouseMotionListener(new MouseAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				controleurVueDynamique.onMouseDrag(e);
			}
		});
	}

	/**
	 * Displays the elements.
	 *
	 * @param g The graphics to paint.
	 */
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		if (this.image != null) {
			this.image = dynModel.getImage();
			if (dynModel.getPosition() != null)
			{
				g.drawImage(image, dynModel.getPosition().x, dynModel.getPosition().y, this);
			}
		}
	}

	/**
	 * Updates the view when the observable has changed.
	 *
	 * @param model The observable model that has changed.
	 */
	@Override
	public void update(Object model)
	{
		if (model instanceof PerspectiveDynamiqueModel)
		{
			dynModel = (PerspectiveDynamiqueModel) model;
			this.image = dynModel.getImage();
			Image updatedImage = image.getScaledInstance(
					dynModel.getLargeur(),
					dynModel.getHauteur(),
					Image.SCALE_AREA_AVERAGING
			);
			this.image = updatedImage;
			this.repaint();
		}
	}

	/**
	 * Returns the controler.
	 *
	 * @return The controler.
	 */
	public ControleurVueDynamique getDynControleur() {
		return dynControleur;
	}

	/**
	 * Assigns a value to the controler.
	 *
	 * @param dynControleur The controler to assign.
	 */
	public void setDynControleur(ControleurVueDynamique dynControleur) {
		this.dynControleur = dynControleur;
	}

	/**
	 * Returns the model.
	 *
	 * @return The model.
	 */
	public PerspectiveDynamiqueModel getDynModel() {
		return dynModel;
	}

	/**
	 * Assigns a value to the model.
	 *
	 * @param dynModel The model to assign.
	 */
	public void setDynModel(PerspectiveDynamiqueModel dynModel) {
		this.dynModel = dynModel;
	}

	/**
	 * Asisgns a border style to the current border.
	 *
	 * @param isActiveView Indicates if this is the active view.
	 */
	public void setCurrBorder(boolean isActiveView) {
		this.currBorder = isActiveView ? activeViewBorder : inactiveViewBorder;
		setBorder(this.currBorder);
	}
}
