/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: ICommand.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/

package commandes;

/**
 * This interface provides the basic methods for a command that comes from a specific image panel.
 *
 */
public interface ICommand
{
	/**
	 * The method to execute the command.
	 *
	 */
	void execute();
	/**
	 * The method to undo the command.
	 *
	 */
	void undo();
}
