/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: LoadCommand.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/

package commandes;

import controleur.MainControleur;
import modele.IModele;
import modele.PerpectiveStatiqueModel;
import modele.PerspectiveDynamiqueModel;
import vue.FramePrincipal;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.*;
import java.util.ArrayList;

/**
 * This class is responsible for the implementation for the load command.
 *
 */
public class LoadCommand implements IGlobalCommand
{
    /**
     * The main view frame.
     */
    private FramePrincipal framePrincipal;
    /**
     * The image to load.
     */
    private transient Image nouvelleImage;

    /**
     * Constructor.
     */
    public LoadCommand(){}

    @Override
    /**
     * Demande au user de sélectionner un fichier, seule l'extension .dat est considérée
     * Va ensuite le désérialiser, et lancer la méthode load avec l'object chargé.
     *
     * @param framePrincipal La vue principale.
     */
    public void execute(FramePrincipal framePrincipal)
    {
        this.framePrincipal = framePrincipal;
        JFileChooser fileChooser = new JFileChooser();
        FileNameExtensionFilter filter = new FileNameExtensionFilter("Fichier", "dat");
        fileChooser.setFileFilter(filter);
        int result = fileChooser.showOpenDialog(null);

        if(result == JFileChooser.APPROVE_OPTION)
        {
            File selectedFile = fileChooser.getSelectedFile();

            try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(selectedFile)))
            {
                Object loadedObject = in.readObject();
                load(loadedObject, framePrincipal.getMainControleur());

            } catch (IOException | ClassNotFoundException e)
            {
                e.printStackTrace();
            }
        }
    }

    /**
     * Charge le nouveau thumbnail et ses perspectives.
     *
     * @param loadedObject Un tableau de grandeur 3 qui contient les trois modeles.
     * @param mainControleur Le controleur principal.
     */
    public void load(Object loadedObject, MainControleur mainControleur)
    {
        var troisModeles = (ArrayList<IModele>) loadedObject;

        for(IModele model: troisModeles)
        {
            System.out.println("Classe = "+ model.getClass().getSimpleName());
            System.out.println("PATH= "+model.getImagePath());
            System.out.println("Position x = " + model.getPosition().x);
            System.out.println("Position y = " + model.getPosition().y);
            System.out.println("Hauteur =" + model.getHauteur());
            System.out.println("Largeur = " + model.getLargeur());


            if (model instanceof PerspectiveDynamiqueModel)
            {
                System.out.println("Init position x =" + ((PerspectiveDynamiqueModel)model).getInitPosition().x);
                System.out.println("Init position y=" + ((PerspectiveDynamiqueModel)model).getInitPosition().y);
            }
        }

        mainControleur.loadImagesFromDatFile(troisModeles);

        for (IModele tmpModel : framePrincipal.getModels())
        {
            String updatedPath = tmpModel.getImagePath();

            if (tmpModel instanceof PerpectiveStatiqueModel)
            {
                PerpectiveStatiqueModel psm = (PerpectiveStatiqueModel) tmpModel;

                try
                {
                    nouvelleImage = ImageIO.read(new File(updatedPath));
                }
                catch (IOException ex) {
                    ex.printStackTrace();
                }

                psm.update(updatedPath, nouvelleImage);
                psm.afficher(nouvelleImage);
            }
            else if (tmpModel instanceof PerspectiveDynamiqueModel)
            {
                PerspectiveDynamiqueModel pdm = (PerspectiveDynamiqueModel) tmpModel;

                try
                {
                    nouvelleImage = ImageIO.read(new File(updatedPath));
                }
                catch (IOException ex)
                {
                    ex.printStackTrace();
                }

                pdm.update(updatedPath, nouvelleImage);

                // Set the new dimensions
                int imgWidth = pdm.getLargeur();
                int imgHeight = pdm.getHauteur();

                pdm.updateImageSize(imgWidth, imgHeight);

                pdm.afficher();
            }
        }
    }
}

