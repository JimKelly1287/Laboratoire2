/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: Observable.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package observer;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * This abstract class provides the basic methods to implement an observable subject as in the Observer design pattern.
 *
 */
public abstract class Observable implements Serializable {
	/**
	 * La liste d'observateurs pour ce sujet observable.
	 */
	ArrayList<Observer> observers = new ArrayList<Observer>(); //liste d'observers

	/**
	 * Constructor.
	 */
	public Observable(){}

	/**
	 * Permet d'attacher un Observer.
	 *
	 * @param observer observateur
	 */
	public void attachObserver(Observer observer) {
		observers.add(observer);
	}

	/**
	 * Sert à avertir tous les observers.
	 *
	 * @param model The model representing the observable.
	 */
	public void notifyObservers(Object model) {
		for (Observer observer : observers) {
			observer.update(model);
		}
	}

}
