/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: PerspectiveDynamiqueModel.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package modele;

import commandes.Memento;
import observer.Observable;

import java.awt.*;
import java.io.Serializable;

/**
 * This class implements the model for the two perspective images.
 *
 */
public class PerspectiveDynamiqueModel extends Observable implements IModele, Serializable
{
	private static final long serialVersionUID = 7105912209944547835L;

	/**
     * The unsigned zoom scale.
     */
    private final int UNSIGNED_ZOOM_SCALE = 25;
	/**
	 * The image path.
	 */
    private String imagePath;
	/**
	 * The initial position of the image.
	 */
	private Point initPosition = new Point(0, 0);
	/**
	 * The current position of the image.
	 */
	private Point position = new Point(0, 0);
	/**
	 * The height of the image.
	 */
	private int hauteur;
	/**
	 * The width of the image.
	 */
    private int largeur;
	/**
	 * The image.
	 */
    private transient Image image;
//    private boolean isNewImage = false;
	/**
	 * Indicates that the model pertains to the active view.
	 */
    private transient boolean representsActiveView = false; //default selected view is the static one.
	/**
	 * The next position of the image in a translation.
	 */
	private Point nextPosition;

	/**
	 * Constructor.
	 *
	 */
	public PerspectiveDynamiqueModel() {
//		this.position.x = 0;
//		this.position.y = 0;
	}

	/**
	 * Constructor.
	 *
	 * @param x The x coordinates of the position.
	 * @param y The y coordinates of the position.
	 */
	public PerspectiveDynamiqueModel(int x, int y) {
		this.position.x = x;
		this.position.y = y;
	}
	/**
	 * This returns the absolute path of the image.
	 *
	 * @return The image absolute path.
	 *
	 */
	@Override
    public String getImagePath() {
        return imagePath;
    }
	/**
	 * This method sets the image absolute path.
	 *
	 * @param imagePath The absolute path of the image.
	 */
	@Override
    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }
	/**
	 * This method sets the image absolute path.
	 *
	 * @param path The absolute path of the image.
	 * @param image The new image.
	 *
	 */
    public void update(String path, Image image)
    {
        this.image = image;
        this.imagePath = path;
    }
	/**
	 * Display the image.
	 *
	 * @param nouvelleImage The image to display.
	 */
	public void afficher(Image nouvelleImage)
	{
		this.largeur = nouvelleImage.getWidth(null);
		this.hauteur = nouvelleImage.getHeight(null);
		this.image = nouvelleImage;
		notifyObservers(this);
	}
	/**
	 * Updates the image height, width and path only.
	 *
	 * @param path The new image path to assign.
	 * @param image The new image.
	 * @param hauteur The height of the image.
	 * @param largeur The width of the image.
	 */
    public void update(String path, Image image, int hauteur, int largeur)
    {
        this.image = image;
        this.imagePath = path;
        this.hauteur = hauteur;
        this.largeur = largeur;
    }
	/**
	 * This returns the position of the image.
	 *
	 * @return The point representing the position.
	 *
	 */
	@Override
	public Point getPosition() {
		return position;
	}
	/**
	 * Display the image.
	 */
    public void afficher()
    {
        notifyObservers(this);
    }
	/**
	 * Assigns a value to the image position.
	 *
	 * @param position The position of the image
	 * @param notifyObserversNow Indicates if the observers must be notified.
	 */
	@Override
	public void setPosition(Point position, boolean notifyObserversNow) {
		this.position = position;

		if (notifyObserversNow)
		{
			notifyObservers(this);

		}
	}
	/**
	 * Returns the height of the image.
	 *
	 * @return The int value of the height.
	 */
	@Override
	public int getHauteur() {
		return hauteur;
	}
	/**
	 * Assigns a value to the height.
	 *
	 * @param hauteur The int value of the height.
	 */
	@Override
	public void setHauteur(int hauteur) {
		this.hauteur = hauteur;
	}
	/**
	 * Returns the width of the image.
	 *
	 * @return The int value of the width.
	 */
	@Override
	public int getLargeur() {
		return largeur;
	}
	/**
	 * Assigns a value to the width.
	 *
	 * @param largeur The int value of the width.
	 */
	@Override
	public void setLargeur(int largeur) {
		this.largeur = largeur;
	}
	/**
	 * Returns the initial position.
	 *
	 * @return The point representing the initial position.
	 */
	public Point getInitPosition()
	{
		return initPosition;
	}

	/**
	 * Returns the next position.
	 *
	 * @return The point representing the next position.
	 */
	public Point getNextPosition() {
		return nextPosition;
	}

	/**
	 * Assigns a point value to the next position.
	 *
	 * @param nextPosition The next position.
	 */
	public void setNextPosition(Point nextPosition) {
		this.nextPosition = nextPosition;
	}
	/**
	 * Returns the image for this model.
	 *
	 * @return The image.
	 */
	public Image getImage() {
		return image;
	}

	/**
	 * Indicates if the associated view is the active one.
	 *
	 * @return True if the linked view is active, otherwise false.
	 */
	public boolean representsActiveView() {
		return representsActiveView;
	}

	/**
	 * Assigns a boolean value to indicate if this model pertains to the active view.
	 *
	 * @param selected If the linked view is the selected one.
	 */
	public void setRepresentsActiveView(boolean selected) {
		representsActiveView = selected;
	}

	/**
	 * Returns the zoom scale.
	 *
	 * @return The zoom scale.
	 */
	public int get_UNSIGNED_ZOOM_SCALE() {
		return UNSIGNED_ZOOM_SCALE;
	}

	/**
	 * Updates the image and its size.
	 *
	 * @param width The new width.
	 * @param height The new height.
	 */
	public void updateImageSize(int width, int height) {
		this.image = image.getScaledInstance(width, height, Image.SCALE_AREA_AVERAGING);
		this.largeur = width;
		this.hauteur = height;
		notifyObservers(this);
	}

	/**
	 * Saves the actuel state as a memento.
	 *
	 * @return The memento representing the state.
	 */
	public Memento saveState() {
		return new Memento(new PerspectiveDynamiqueModel(this.getPosition().x,this.getPosition().y));
	}

	/**
	 * Loads a memento.
	 *
	 * @param memento The memento to restore.
	 */
	public void restoreState(Memento memento) {
		this.setPosition(new Point(memento.getState().getPosition()), true);
		notifyObservers(this);
	}

	/**
	 * The customized string value of the model.
	 *
	 * @return The string representing the model.
	 */
	@Override
	public String toString() {
		return "PerspectiveDynamiqueModel{" +
				"position=" + position +
				", hauteur=" + hauteur +
				", largeur=" + largeur +
				", image=" + image +
				'}';
	}
	/**
	 * Method that reinitialize the dimensions and position of the image.
	 */
	public void reinitialize()
	{
		this.imagePath = null;
		this.position = new Point(0,0);
		this.hauteur = 0;
		this.largeur = 0;
		this.image = null;
		this.nextPosition = null;
		representsActiveView = false;
	}
}