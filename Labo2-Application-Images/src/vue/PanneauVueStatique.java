/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: PanneauVueStatique.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package vue;

import controleur.ControleurVueStatique;
import modele.PerpectiveStatiqueModel;
import modele.PerspectiveDynamiqueModel;
import observer.Observer;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.io.Serializable;

/**
 * This class implements the view panel for the thumbnail image.
 *
 */
public class PanneauVueStatique extends JPanel implements Observer, Serializable {

	/**
	 * The associated controler.
	 */
	private  ControleurVueStatique controleurVueStatique;
	/**
	 * The associated model.
	 */
	private PerpectiveStatiqueModel psm;
	/**
	 * The displayed image.
	 */
	private transient Image image;
	/**
	 * The border style.
	 */
	private  Border currBorder = BorderFactory.createEtchedBorder(EtchedBorder.LOWERED);

	/**
	 * Constructor.
	 *
	 * @param controleurVueStatique The associated controler.
	 */
	public PanneauVueStatique(ControleurVueStatique controleurVueStatique) {
		this.controleurVueStatique = controleurVueStatique;
		this.setVisible(true);
		setBorder(currBorder);

	}

//	public void updateFromLoad(PanneauVueStatique loadedObject){
//		if(loadedObject instanceof PanneauVueStatique){
//			PerpectiveStatiqueModel loadedModel = (PerpectiveStatiqueModel) loadedObject.getStaModel();
//			this.image = loadedModel.getImage();
//
//			this.repaint();
//		}
//	}

	/**
	 * Displays the elements.
	 *
	 * @param g The graphics to paint.
	 */
	@Override
	protected void paintComponent(Graphics g)
	{
		super.paintComponent(g);
		if (this.image != null) {
			this.image = psm.getImage();
			Point pos = psm.getPosition();
			g.drawImage(image, pos.x, pos.y, this);
		}
	}

	/**
	 * Updates the view when the observable has changed.
	 *
	 * @param model The observable model that has changed.
	 */
	@Override
	public void update(Object model)
	{
		psm = (PerpectiveStatiqueModel) model;
		this.image = psm.getImage();
		this.repaint();
	}

	/**
	 * Returns the associated model.
	 *
	 * @return The model.
	 */
	public PerpectiveStatiqueModel getStaModel()
	{
		return psm;
	}
}
