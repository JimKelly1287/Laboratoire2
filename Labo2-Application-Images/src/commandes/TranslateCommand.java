/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: TranslateCommand.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package commandes;

import modele.PerspectiveDynamiqueModel;
import vue.PanneauVueDynamique;

import java.awt.*;
import java.io.Serializable;

/**
 * This class is responsible for the translation command.
 */
public class TranslateCommand implements ICommand, Serializable {
	/**
	 * The perspective view.
	 */
	private final PanneauVueDynamique panneauVueDynamique;
	/**
	 * The initial position of the image before translation.
	 */
	private Point initPosition;
	/**
	 * The final position of the image after translation.
	 */
	private Point finalPosition;

	/**
	 * Constructor.
	 *
	 * @param panneauVueDynamique The perspective view.
	 * @param initPosition The initial position of the image.
	 */
	public TranslateCommand(PanneauVueDynamique panneauVueDynamique, Point initPosition) {
		this.panneauVueDynamique = panneauVueDynamique;
		this.initPosition = initPosition;
	}

	/**
	 * This method execute the translate command.
	 */
	@Override
	public void execute() {
		Point center = panneauVueDynamique.getDynControleur().getCenterOfImage();
		panneauVueDynamique.getDynModel().setPosition(panneauVueDynamique.getDynModel().getNextPosition(), true);
	}

	/**
	 * The method to undo the command.
	 */
	@Override
	public void undo()
	{
		PerspectiveDynamiqueModel modele =  panneauVueDynamique.getDynModel();

		Point tempPos = this.initPosition;
		this.initPosition = this.finalPosition;
		this.finalPosition = tempPos;

		modele.setPosition(this.finalPosition, true);

		if (!GestionnaireCommandes.getMementosList().isEmpty())
		{
			if (GestionnaireCommandes.getMementosList().size() > 1)
			{
				GestionnaireCommandes.getMementosList().pop();
				panneauVueDynamique.getDynModel().restoreState(GestionnaireCommandes.getMementosList().lastElement());
			} if (GestionnaireCommandes.getMementosList().size() == 1) {
				panneauVueDynamique.getDynModel().restoreState(new Memento(new PerspectiveDynamiqueModel()));
			}
		}
		else
		{
			System.out.println("Dans Translate command undo() pour else");
		}

	}

	/**
	 * This returns the initial position.
	 *
	 * @return The Point representing the initial position.
	 */
	public Point getInitPosition()
	{
		return initPosition;
	}

	/**
	 * This assigns a Point value to the initial position.
	 *
	 * @param initPosition The initial position as Point.
	 */
	public void setInitPosition(Point initPosition)
	{
		this.initPosition = initPosition;
	}

	/**
	 * This returns the final position.
	 *
	 * @return The Point representing the final position.
	 */
	public Point getFinalPosition()
	{
		return finalPosition;
	}

	/**
	 * This assigns a Point value to the final position.
	 *
	 * @param finalPosition The final position as Point.
	 */
	public void setFinalPosition(Point finalPosition)
	{
		this.finalPosition = finalPosition;
		System.out.println("Finalisation de translate command");
		System.out.println("initPosition = "+ this.initPosition);
		System.out.println("finalPosition = "+ this.finalPosition);
		System.out.println("Finalisation de translate command");
	}
}
