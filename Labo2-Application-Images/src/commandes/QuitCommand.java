/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: QuitCommand.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/

package commandes;

import javax.swing.*;

/**
 * This class is responsible for the implementation for the quit command.
 *
 */
public class QuitCommand implements ICommand {

	/**
	 * Constructor.
	 */
	public QuitCommand(){}
	/**
	 * 	This method execute the quit command.
	 */
	@Override
	public void execute() {
		JDialog.setDefaultLookAndFeelDecorated(true);
		int response = JOptionPane.showConfirmDialog(null, "Voulez vous quitter ?", "Quitter",
				JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
		if (response == JOptionPane.YES_OPTION) {
			System.exit(0);
		}
	}

	/**
	 * The method to undo the command.
	 *
	 */
	@Override
	public void undo() {

	}
}
