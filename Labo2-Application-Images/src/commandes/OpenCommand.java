/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: OpenCommand.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package commandes;

import modele.*;
import vue.FramePrincipal;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.io.File;
import java.io.IOException;

/**
 * This class is responsible for the implementation for the open command.
 *
 */
public class OpenCommand implements IGlobalCommand {

	/**
	 * The new image to open.
	 */
	private transient Image nouvelleImage;
	/**
	 * Constructor.
	 */
	public OpenCommand(){}

	/**
	 * This method execute the open command.
	 *
	 * @param framePrincipal The main frame that contains the whole application GUI.
	 */
	@Override
	public void execute(FramePrincipal framePrincipal)
	{
		JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView().getHomeDirectory());
		fileChooser.setDialogTitle("Selectionnez une image");
		fileChooser.setAcceptAllFileFilterUsed(false);

		FileNameExtensionFilter filtre = new FileNameExtensionFilter("Image",
				"jpg", "jpeg", "png", "gif");
		fileChooser.addChoosableFileFilter(filtre);

		int returnValue = fileChooser.showOpenDialog(null);

		if (returnValue == JFileChooser.APPROVE_OPTION) {
			File selectedFile = fileChooser.getSelectedFile();
			String imageFilePath = selectedFile.getAbsolutePath();

			try {
				nouvelleImage = ImageIO.read(new File(imageFilePath));
			} catch (IOException ex) {
				ex.printStackTrace();
			}

			for (IModele modele : framePrincipal.getModels())
			{
				modele.setImagePath(imageFilePath);

				if (modele instanceof PerpectiveStatiqueModel)
				{
					PerpectiveStatiqueModel psm = (PerpectiveStatiqueModel) modele;
					psm.update(imageFilePath, nouvelleImage);
					psm.afficher(nouvelleImage);
				}
				else if (modele instanceof PerspectiveDynamiqueModel)
				{
					PerspectiveDynamiqueModel pdm = (PerspectiveDynamiqueModel) modele;
					pdm.update(imageFilePath, nouvelleImage, nouvelleImage.getHeight(null), nouvelleImage.getWidth(null));
					pdm.afficher(nouvelleImage);

				}
			}
		}
	}
}

