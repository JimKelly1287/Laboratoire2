/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: ControleurVueDynamique.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package controleur;

import commandes.*;
import modele.PerspectiveDynamiqueModel;
import vue.PanneauVueDynamique;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.io.Serializable;

/**
 * This class implements the controler for a perspective view, with a modifiable image.
 */
public class ControleurVueDynamique extends MouseAdapter implements Serializable {
	/**
	 * The (x,y) coordinates that represents where the mouse is clicked.
	 */
	public static Point clicActuel;
	/**
	 * The model linked to this controler.
	 */
	private PerspectiveDynamiqueModel dynModel;
	/**
	 * The view linked to this controler.
	 */
	private PanneauVueDynamique vueDyn;
	/**
	 * Represents the current translation command.
	 */
	private TranslateCommand translateCommand;
	/**
	 * Represents the initial position of the mouse at the beginning of dragging.
	 */
	private Point firstMousePositionAtDragging;
	/**
	 * Indicates that a translation is occurring.
	 */
	private boolean isCurrentlyTranslating = false;

	/**
	 * Constructor.
	 *
	 * @param pdm The model to link.
	 */
	public ControleurVueDynamique(PerspectiveDynamiqueModel pdm) {
		this.dynModel = pdm;
	}

	/**
	 * Handles the mouse press (mouse down) event.
	 *
	 * @param e The mouse event.
	 */
	public void onMousePress(MouseEvent e) {
		// MainControleur makes sure to activate/deactivate the proper panel.
		MainControleur.onMousePress(e, this.vueDyn);
	}

	/**
	 * Handles the rotation of the mouse wheel event, for the zoom command.
	 *
	 * @param e The mouse wheel event.
	 */
	public void onMouseWheel(MouseWheelEvent e) {
		// MainControleur makes sure to activate/deactivate the proper panel.
		MainControleur.onMouseWheel(e, this.vueDyn);

		if (this.dynModel.representsActiveView() && this.dynModel.getImage() != null) {
			int rotation = e.getWheelRotation();
			int signedZoomScale = -(rotation) * dynModel.get_UNSIGNED_ZOOM_SCALE();

			// Create, execute and store the new zoom command
			ICommand zoomCommand = new ZoomCommand(vueDyn, signedZoomScale);
			zoomCommand.execute();
			GestionnaireCommandes.push(zoomCommand);
		}
	}

	/**
	 * Handles the dragging of the mouse event, for the translation command.
	 *
	 * @param e The mouse event.
	 */

	public void onMouseDrag(MouseEvent e) {
		if (SwingUtilities.isLeftMouseButton(e)) {
			clicActuel = e.getPoint();
			System.out.println("click actuel x = "+ clicActuel.x + "   Click actuel y="+ clicActuel.y);
			Point currImagePosition = vueDyn.getDynModel().getPosition();
			System.out.println("currPosition actuel x = "+ currImagePosition.x + "   currPosition actuel y="+ currImagePosition.y);

			// Begins a new translation
			if (!this.isCurrentlyTranslating)
			{
				this.isCurrentlyTranslating = true;
				this.firstMousePositionAtDragging = clicActuel;

				//Create a new command
				this.translateCommand = new TranslateCommand(vueDyn, currImagePosition);
			}

			dynModel.setNextPosition(getCenterOfImage());
			this.translateCommand.execute();
			dynModel.setNextPosition(getCenterOfImage());
		}
	}

	/**
	 * Find the center of image with cursor, for the translation command.
	 *
	 * @return The center of the image.
	 */
	public Point getCenterOfImage() {
		return new Point((int) (clicActuel.getX() - vueDyn.getDynModel().getImage().getWidth(null) / 2),
				(int) (clicActuel.getY() - vueDyn.getDynModel().getImage().getHeight(null) / 2));
	}
	/**
	 * Handles the release mouse event, for the translation command.
	 *
	 * @param e The mouse event.
	 */
	public void onMouseRelease(MouseEvent e) {

		int newX = dynModel.getPosition().x;
		int newY = dynModel.getPosition().y;

		GestionnaireCommandes.getMementosList().push(new PerspectiveDynamiqueModel(newX, newY).saveState());

		//Confirms that this is the end of a translation
		if (this.isCurrentlyTranslating)
		{
			this.translateCommand.setFinalPosition(new Point(newX, newY));
			GestionnaireCommandes.push(translateCommand);
			// Reset command to null
			this.isCurrentlyTranslating = false;
			this.translateCommand = null;
		}

//		GestionnaireCommandes.getMementosList().push(new PerspectiveDynamiqueModel(dynModel.getPosition().x, dynModel.getPosition().y).saveState());

		System.out.println("Saved State at position: " + dynModel.getPosition());
		for (int i = 0; i < GestionnaireCommandes.getMementosList().size(); i++) {
			System.out.println(i + ": "+ GestionnaireCommandes.getMementosList().get(i).getState().getPosition());
		}
	}

	/**
	 * Returns the view.
	 *
	 * @return The view.
	 */
	public PanneauVueDynamique getVueDyn() {
		return this.vueDyn;
	}

	/**
	 * Assigns a view to this controler.
	 *
	 * @param vueDyn The view to assign.
	 */
	public void setVueDyn(PanneauVueDynamique vueDyn) {
		this.vueDyn = vueDyn;
	}

	/**
	 * Returns the model.
	 *
	 * @return The model.
	 */
	public PerspectiveDynamiqueModel getDynModel() {
		return this.dynModel;
	}
}
