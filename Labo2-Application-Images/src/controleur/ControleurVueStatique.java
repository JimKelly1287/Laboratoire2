/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: ControleurVueStatique.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/

package controleur;

import modele.PerpectiveStatiqueModel;

import java.io.Serializable;

/**
 * This class implements the controler for the thumbnail view, with the static image.
 */
public class ControleurVueStatique implements Serializable {
	/**
	 * The model linked to this controler.
	 */
	private PerpectiveStatiqueModel perpectiveStatiqueModel;
	/**
	 * Constructor.
	 *
	 * @param psm The associated model.
	 */
	public ControleurVueStatique(PerpectiveStatiqueModel psm) {
		this.perpectiveStatiqueModel = psm;
	}

	/**
	 * Returns the model.
	 *
	 * @return The model.
	 */
	public PerpectiveStatiqueModel getPerpectiveStatiqueModel() {
		return perpectiveStatiqueModel;
	}
}
