/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: Memento.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package commandes;

import modele.PerspectiveDynamiqueModel;

import java.io.Serializable;

/**
 * This class implements the memento as in the memento design pattern.
 */
public class Memento implements Serializable {

	/**
	 * The saved model.
	 */
	private PerspectiveDynamiqueModel pdm;

	/**
	 * Constructor.
	 *
	 * @param pdm The model to save.
	 */
	public Memento(PerspectiveDynamiqueModel pdm) {
		this.pdm = pdm;
	}

	/**
	 * This returns the saved model.
	 *
	 * @return The saved model.
	 */
	public PerspectiveDynamiqueModel getState() {
		return pdm;
	}

}
