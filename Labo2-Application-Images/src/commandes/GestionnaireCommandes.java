/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: GestionnaireCommandes.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/

package commandes;
import java.io.Serializable;
import java.util.Stack;

/**
 * This class represent the implementation of the Command design pattern
 * and is represented as a singleton.
 * It contains the history of commands as a stack.
 */
public class GestionnaireCommandes implements Serializable {
	/**
	 * Returns the history of commands.
	 *
	 * @return The singleton instance of CommandHistory.
	 */
	public static GestionnaireCommandes getInstance() {
		if (gestionnaireCommandes == null) {
			return new GestionnaireCommandes();
		}
		return gestionnaireCommandes;
	}

	/**
	 * The singleton instance of GestionnaireCommandes.
	 */
	private static GestionnaireCommandes gestionnaireCommandes;

	/**
	 * Constructor.
	 */
	private GestionnaireCommandes(){}
	/**
	 * Stack of the commands.
	 */
	private static Stack<ICommand> commandHistory = new Stack<ICommand>();
	/**
	 * Stack of the states of the images.
	 */
	private static Stack<Memento> mementosList = new Stack<Memento>();
	/**
	 * Returns the mementos.
	 *
	 * @return The stack of mementos.
	 */
	public static Stack<Memento> getMementosList() {
		return mementosList;
	}
	/**
	 * Adds a command on top of the stack.
	 *
	 * @param command The command to add.
	 */
	public static void push(ICommand command) {
		commandHistory.push(command);
	}
	/**
	 * Returns the command on top of the stack,
	 * then removes it.
	 *
	 * @return The command on top of the stack.
	 */
	public static ICommand pop() {
		return commandHistory.pop();
	}
    /**
     * Returns the stack of commands.
     *
     * @return The stack of commands.
     */
    public static Stack<ICommand> getCommandHistory() {
        return commandHistory;
    }

}
