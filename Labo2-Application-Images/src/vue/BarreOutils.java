/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: BarreOutils.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package vue;


import controleur.ControleurBarreOutils;

import javax.swing.*;
import java.awt.*;

/**
 * This class implements the view for the toolbar menu.
 *
 */
public class BarreOutils extends JMenuBar {
	/**
	 * The text displayed for menu option Fichier.
	 */
	private static final String MENU_FICHIER_TITRE = "Fichier";
	/**
	 * The text displayed for menu option Editer.
	 */
	private static final String MENU_EDITER_TITRE = "Éditer";
	/**
	 * The text displayed for menu option Ouvrir.
	 */
	private static final String MENU_FICHIER_OUVRIR = "Ouvrir";
	/**
	 * The text displayed for menu option Charger.
	 */
	private static final String MENU_FICHIER_CHARGER = "Charger";
	/**
	 * The text displayed for menu option Undo.
	 */
	private static final String MENU_FICHIER_UNDO = "Undo";
	/**
	 * The text displayed for menu option Enregistrer.
	 */
	private static final String MENU_FICHIER_SAVE = "Enregistrer";
	/**
	 * The text displayed for menu option Quitter.
	 */
	private static final String MENU_FICHIER_QUITTER = "Quitter";
	/**
	 * The toolbar controler.
	 */
	private ControleurBarreOutils controleurBarreOutils;
	/**
	 * The main view container.
	 */
	private final FramePrincipal framePrincipal;

	/**
	 * Constructor.
	 *
	 * @param framePrincipal The main container view.
	 * @param controleur The toolbar controler.
	 */
	public BarreOutils(FramePrincipal framePrincipal, ControleurBarreOutils controleur)
	{
		this.framePrincipal = framePrincipal;
		this.controleurBarreOutils = controleur;
		initialiserBarreOutils();
	}

	/**
	 * Creates the toolbar menu.
	 */
	private void initialiserBarreOutils() {

		setPreferredSize(new Dimension(1200, 50));

		Font fontBarreOutil = new Font(Font.SERIF, Font.BOLD, 16);

		JMenu menuFichier = new JMenu(MENU_FICHIER_TITRE);
		menuFichier.setFont(fontBarreOutil);

		JMenuItem menuOuvrir = new JMenuItem(MENU_FICHIER_OUVRIR);
		JMenuItem menuLoad = new JMenuItem(MENU_FICHIER_CHARGER);
		JMenuItem menuSave = new JMenuItem(MENU_FICHIER_SAVE);
		JMenuItem menuQuitter = new JMenuItem(MENU_FICHIER_QUITTER);

		JMenu menuEdit = new JMenu(MENU_EDITER_TITRE);
		menuEdit.setFont(fontBarreOutil);

		JMenuItem menuUndo = new JMenuItem(MENU_FICHIER_UNDO);
//		JMenuItem menuRedo = new JMenuItem("Redo     (non implemente)");

		menuOuvrir.setFont(fontBarreOutil);
		menuLoad.setFont(fontBarreOutil);
		menuQuitter.setFont(fontBarreOutil);
		menuSave.setFont(fontBarreOutil);
		menuUndo.setFont(fontBarreOutil);
//		menuRedo.setFont(fontBarreOutil);

		menuFichier.addActionListener(controleurBarreOutils);
		menuOuvrir.addActionListener(controleurBarreOutils);
		menuLoad.addActionListener(controleurBarreOutils);
		menuSave.addActionListener(controleurBarreOutils);
		menuQuitter.addActionListener(controleurBarreOutils);
		menuEdit.addActionListener(controleurBarreOutils);
		menuUndo.addActionListener(controleurBarreOutils);
//		menuRedo.addActionListener(controleurBarreOutils);

		menuFichier.add(menuOuvrir);
		menuFichier.add(menuLoad);
		menuFichier.add(menuSave);
		menuFichier.add(menuQuitter);

		menuEdit.add(menuUndo);
//		menuEdit.add(menuRedo);

		this.add(menuFichier);
		this.add(menuEdit);
	}
}
