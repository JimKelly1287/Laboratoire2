/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: Observer.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package observer;

/**
 * This interface provides the basic methods to implement an observer as in the Observer design pattern.
 *
 */
public interface Observer {
	/**
	 * The method to implement when the observable has changed.
	 *
	 * @param model The observable model that has changed.
	 *
	 */
	void update(Object model);

}
