/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: ControleurBarreOutils.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package controleur;

import commandes.*;
import vue.BarreOutils;
import vue.FramePrincipal;

import javax.naming.ldap.Control;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;

/**
 * This class implements the controler for the toolbar menu.
 */
public class ControleurBarreOutils implements ActionListener, Serializable
{
	/**
	 * The main container view.
	 */
	private FramePrincipal framePrincipal;
	/**
	 * The toolbar menu.
	 */
	private BarreOutils barreOutils;

	/**
	 * Constructor.
	 *
	 * @param framePrincipal The main container view.
	 */
	public ControleurBarreOutils(FramePrincipal framePrincipal)
	{
		this.framePrincipal = framePrincipal;
	}

	/**
	 * The event handler for an event happening in the toolbar menu.
	 *
	 * @param e the event to be processed.
	 */
	@Override
	public void actionPerformed(ActionEvent e) {
		switch (e.getActionCommand()) {
			case "Ouvrir":
				framePrincipal.reinitializeModels();
				OpenCommand o = new OpenCommand();
				o.execute(framePrincipal);
				break;
			case "Charger":
				LoadCommand l = new LoadCommand();
				l.execute(framePrincipal);
				break;
			case "Enregistrer":
				SaveCommand s = new SaveCommand();
				s.execute(framePrincipal);
				break;
			case "Undo":
				if (GestionnaireCommandes.getCommandHistory().isEmpty())
				{
					System.out.println("EMPTY COMMAND LIST");
				}
				else
				{
					GestionnaireCommandes.getCommandHistory().pop().undo();
				}
				break;
			case "Quitter":
				QuitCommand q = new QuitCommand();
				q.execute();
				break;
		}
	}

	/**
	 * This assigns a toolbar view to the controler.
	 *
	 * @param barreOutils The toolbar view.
	 */
	public void setBarreOutils(BarreOutils barreOutils) {
		this.barreOutils = barreOutils;
	}
}
