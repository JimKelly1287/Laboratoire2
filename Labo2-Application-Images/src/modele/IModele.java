/******************************************************
 Cours:   LOG121
 Session: A2013
 Groupe:
 Projet: Laboratoire #2
 Étudiant(e)s: Jim-Kelly Clervoyant, Philippe Lalancette, Makraphone Phouttama
 Professeur : Benoît Galarneau
 Assistant de laboratoire: Bilal Alchalabi
 Nom du fichier: IModele.java
 Date créé: 2023-11-10
 Date dern. modif. 2023-12-05
 *******************************************************
 Historique des modifications
 *******************************************************
 2023-11-10 Version initiale
 2023-12-07 Finalisation
 *******************************************************/
package modele;

import java.awt.*;
import java.io.Serializable;

/**
 * This interface provides the basic methods to implement in a model, in the MVC pattern.
 *
 */
public interface IModele extends Serializable{
    /**
     * This returns the absolute path of the image.
     *
     * @return The image absolute path.
     */
    public String getImagePath();
    /**
     * This method sets the image absolute path.
     *
     * @param imagePath The absolute path of the image.
     */
    public void setImagePath(String imagePath);
    /**
     * This returns the position of the image.
     *
     * @return The point representing the position.
     *
     */
    public Point getPosition();

    /**
     * Assigns a value to the image position.
     *
     * @param position The position of the image
     * @param notifyObserversNow Indicates if the observers must be notified.
     */
    public void setPosition(Point position, boolean notifyObserversNow);

    /**
     * Returns the height of the image.
     *
     * @return The int value of the height.
     */
    public int getHauteur();

    /**
     * Assigns a value to the height.
     *
     * @param hauteur The int value of the height.
     */
    public void setHauteur(int hauteur);
    /**
     * Returns the width of the image.
     *
     * @return The int value of the width.
     */
    public int getLargeur();
    /**
     * Assigns a value to the width.
     *
     * @param largeur The int value of the width.
     */
    public void setLargeur(int largeur);
    /**
     * Method that reinitialize the dimensions and position of the image.
     */
    public void reinitialize();
}
